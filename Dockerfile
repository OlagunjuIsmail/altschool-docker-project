FROM php:8.0.2-fpm

# Arguments defined in docker-compose.yml
ARG user
ARG uid

# Install system dependencies
#RUN apt-get update && apt-get install -y \
  #  git \
   # curl \
   # libpng-dev \
   # libonig-dev \
   # libxml2-dev \
   # zip \
   # unzip

RUN apt update && apt add --no-cache \
    build-base \
    libpng-dev \
    libjpeg-turbo-dev \
    libmcrypt-dev \
    libxml2-dev \
    libzip-dev 

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

#RUN docker-php-ext-configure gd --with-jpeg \
    #&& docker-php-ext-install -j$(nproc) gd \
   # && docker-php-ext-install bcmath \
   # && docker-php-ext-install mbstring \
   # && docker-php-ext-install pdo \
   # && docker-php-ext-install pdo_mysql \
   # && docker-php-ext-install soap \
   # && docker-php-ext-install xml \
   # && docker-php-ext-install zip

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Create system user to run Composer and Artisan Commands
RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user

# Set working directory
WORKDIR /var/www

USER $user