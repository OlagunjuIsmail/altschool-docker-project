# Docker Environment for PHP, Nginx and MySQL

This repository contains the necessary files to run a PHP app using Nginx as a web server and MySQL as a database.

## Requirements
- Docker
- Docker Compose

## Getting Started
1. Clone this repository https://github.com/laravel/laravel.git laravel-app to your local machine.
2. Open terminal and navigate to the cloned repository.
3. Run the following command to build the images and start the containers:
    ```
    docker-compose up -d
    ```
4. Verify that the containers are running by using the following command:
    ```
    docker ps
    ```
5. Access the PHP app in your web browser by visiting http://localhost.

## Dockerfile
The Dockerfile in this repository is used to build the PHP image. It uses the official PHP image and adds some additional configurations.

## Docker Compose
The `docker-compose.yml` file defines the services for the PHP app, Nginx web server, and MySQL database. It also creates a shared network for the containers to communicate and uses volumes to persist data.

## Configuration
You can modify the environment variables and other configurations in the `docker-compose.yml` file as per your needs.

## Clean Up
To stop the containers and remove the images, run the following command:
    ```
    docker-compose down
    ```

## Conclusion
This setup provides a simple and efficient way to run a PHP app with Nginx and MySQL. You can easily customize it to meet your specific requirements.
